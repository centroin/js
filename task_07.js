//  Funtion constructor

// normal object
// var mani = {
//   name: "mani",
//   yearOfBirth: 1998,
//   job: "developer",
// };

// // constructor - Normal function
// var Person = function (name, yearOfBirth, job) {
//   this.name = name;
//   this.yearOfBirth = yearOfBirth;
//   this.job = job;
// };
// // prototype
// Person.prototype.calc = function() {
//   return 2020 - this.yearOfBirth;
// };
// var nirmal = new Person("nirmal", 1998, "owner"); /*instantiation*/
// var nirmalAge = nirmal.calc();
// console.log(nirmalAge);

// // constructor - arrow function
// var Bird = (name, yearOfBirth) => {
//   this.name = name;
//   this.yearOfBirth = yearOfBirth;
// };
// // prototype
// Bird.prototype.calc = function() {
//   return 2020 - this.yearOfBirth;
// };
// var parrot = new Bird("parrot", 1998);
// var parrotAge = parrot.calc();
// console.log(parrot.name);

//  Object.create
var human = {
  calculateAge: function() {
      console.log(2016 - this.yearOfBirth);
  }
};
var personProto = Object.create(human);

var john = Object.create(personProto);
john.name = 'John';
john.yearOfBirth = 1990;
john.job = 'teacher';
var jane = Object.create(personProto, {
  name: { value: 'Jane' },
  yearOfBirth: { value: 1969 },
  job: { value: 'designer' }
});