## Java Script

>1. ### Operator precedence
>> Operators with higher precedence become the operands of operators with lower precedence.(+, /, -, *)
>
>> For detail: [Operator precedence](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence)

>2. ### Hosting
>> Hosting works only for function declaration not for function expressions