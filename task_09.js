function interviewQuestion(job) {
        var designer = "what you design?";
        var teacher = "what you teach?";
        var basic = "what you do?";

        return function(name){
                if ( job === "designer" ){
                        console.log(`${name}, ${designer}`)
                }else if ( job === "teacher" ){
                        console.log(`${name}, ${teacher}`)
                }else {
                        console.log(`${name}, ${basic}`)
                }
        }
}

var designerQuestion = interviewQuestion('designer')('john');
var teacherQuestion = interviewQuestion('designer')('venky');
var basicQuestion = interviewQuestion('normal')('mani');