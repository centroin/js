var john = {
    name : "Vetri John Smith",
    bill:[115, 255, 400, 85, 300],
    calcTips: function(){
        this.tips = [];
        this.totalBill = [];
        for (i=0; i<this.bill.length; i++){
            var percentage;
            var bill = this.bill[i];
            if(bill < 50){
                percentage = 0.05;
            }else if(bill > 50 && bill<200){
                percentage = 0.04;
            }else if(bill > 200 && bill<300){
                percentage = 0.03;
            }else if(bill > 300 && bill<400){
                percentage = 0.02;
            }else{
                percentage = 0.01;
            }
            this.tips[i] = bill * percentage;
            this.totalBill[i] = bill + this.tips[i];
        }
    }
}
// new Object Created
var nick = new Object();
    nick.name = "Nick Pondi";
    nick.bill = [92, 158, 350, 485, 250];
    nick.calcTips = function(){
        this.tips = [];
        this.totalBill = [];
        for (i=0; i<this.bill.length; i++){
            var percentage;
            var bill = this.bill[i];
            if(bill < 50){
                percentage = 0.05;
            }else if(bill > 50 && bill<200){
                percentage = 0.04;
            }else if(bill > 200 && bill<300){
                percentage = 0.03;
            }else if(bill > 300 && bill<400){
                percentage = 0.02;
            }else{
                percentage = 0.01;
            }
            this.tips[i] = bill * percentage;
            this.totalBill[i] = bill + this.tips[i];
        }
    };

// Average Calculator
var calculator = function(tips){
    var sum = 0 ;
    for (i=0; i < tips.length ; i++){
        sum += tips[i];
    }
    return sum
}
john.calcTips();
nick.calcTips();

john.average = calculator(john.tips);
nick.average = calculator(nick.tips);

if(john.average > nick.average){
    console.log("John paid more");
}else if(john.average < nick.average){
    console.log("Nick paid more");
}else{
    console.log("Both paid equally!")
}