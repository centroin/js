let members = [
    {
      id: 1,
      name: "A",
      location: {
        city: "Madurai",
        state: "TN",
        country: "IN",
      },
    },
    {
      id: 2,
      name: "b",
      location: {
        city: "Chennai",
        state: "TN",
        country: "IN",
      },
    },
    {
      id: 3,
      name: "C",
      location: {
        city: "Trichy",
        state: "TN",
        country: "IN",
      },
    },
    {
      id: 4,
      name: "C",
      location: {
        city: "Trichy",
        state: "TN",
        country: "IN",
      },
    },{
      id: 5,
      name: "C",
      location: {
        city: "Trichy",
        state: "TN",
        country: "IN",
      },
    },{
      id: 6,
      name: "C",
      location: {
        city: "Madurai",
        state: "TN",
        country: "IN",
      },
    },
  ];
  
  let cities = [...new Set(members.map(item => item.location.city))];
  
  let cityList = {};
  
  cities.map ((city)=>{
      cityList[city]=members.filter(member=> member.location.city==city) 
  })
  
  console.log(cities,cityList);
  
  
  // 1. Cities Array ['Madurai', 'Chennai', 'Trichy']
  // 2. Prepare object keys dynamically from the above single dimensional array and prepare the group of members with the respective citties.
  // Sample Expected Output:
  /*
  {
  "Madurai": [{
  20 Members Array
  }],
  "Chennai": [{
  20 Members Array
  }],
  "Trichy": [{
  10 Members Array
  }]
  }
  */
  